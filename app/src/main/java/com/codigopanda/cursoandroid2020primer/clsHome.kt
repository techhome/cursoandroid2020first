package com.codigopanda.cursoandroid2020primer

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.fhome.*

class clsHome : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fhome)
        val sharedPref: SharedPreferences = getSharedPreferences(
            "user",
            Context.MODE_PRIVATE
        )
        textNombreX.text = sharedPref.getString("name", "No Se Guardo Correctamente")
            .toString()


        val listMovie = arrayListOf<String>()
        listMovie.add("Joker")
        listMovie.add("1917")
        listMovie.add("Avengers Infinity War")
        listMovie.add("Avengers EndGame")
        listMovie.add("Capitan America")
        listMovie.add("Spiderman")
        listMovie.add("Batman")
        val adapter = ArrayAdapter(this,
            android.R.layout.simple_list_item_1, listMovie)
        listMovieItems.adapter = adapter
        listMovieItems.setOnItemClickListener { adapterView, view, position, l ->
            Toast.makeText(this, listMovie[position], Toast.LENGTH_LONG)
                .show()
        }
    }
}
