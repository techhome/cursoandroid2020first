package com.codigopanda.cursoandroid2020primer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSeriePrimos.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this,clsSeriePrimos::class.java))
        })
    }
}
