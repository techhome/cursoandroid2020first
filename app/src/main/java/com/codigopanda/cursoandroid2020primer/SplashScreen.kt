package com.codigopanda.cursoandroid2020primer

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler






class SplashScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)

        Handler().postDelayed({
            val sharedPref: SharedPreferences = getSharedPreferences("user",
                Context.MODE_PRIVATE)
            if (sharedPref.getBoolean("user", false)) {
                startActivity(Intent(this, clsHome::class.java))
            }else{
                startActivity(Intent(this, clsLogin::class.java))
            }
            finish()
        }, 3000)

    }
}
