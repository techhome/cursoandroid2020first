package com.codigopanda.cursoandroid2020primer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fserieprimos.*

class clsSeriePrimos : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fserieprimos)
        btnGenerar.setOnClickListener(View.OnClickListener {
            var c: Int = 0
            var aux: Int = 1

            var cad: String = ""


            var n: Int = Integer.parseInt(editTextSeriePrimos.text.toString())
            if (n > 0) {
                while (c < n) {
                    if (esPrimo(aux)) {
                        c++
                        cad += aux.toString() + "," //-> cad=cad+aux.toString()+","
                    }
                    aux++
                }
            }



            serieRespuesta.text = cad
        })
    }

    fun esPrimo(num: Int): Boolean {

        var i=2
        var c=0
        while (i<num){
            if(num%i==0){
                c++ // -> c=c+1
                break
            }
            i++
        }

        return c==0
    }
}
