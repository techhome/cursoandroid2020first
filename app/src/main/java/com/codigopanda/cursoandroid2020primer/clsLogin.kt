package com.codigopanda.cursoandroid2020primer

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.flogin.*

class clsLogin : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.flogin)
        buttonLogin.setOnClickListener(View.OnClickListener {
            //Toast.makeText(this,"login",Toast.LENGTH_SHORT).show()
            val email: String = editEmail.text.toString()
            val password: String = editPassword.text.toString()
            if (email == "docker@gmail.com" && password == "123") {

                // Guardando datos de usuario logeado
                val sharedPref:SharedPreferences = getSharedPreferences("user",
                    Context.MODE_PRIVATE)
                val editor = sharedPref.edit()
                editor.putBoolean("user", true)
                editor.putString("name","Docker")
                editor.apply()
                // ---------------------------------------------------

                startActivity(Intent(this,clsHome::class.java))
            } else {
                Toast.makeText(this, "Su correo o contraseña son incorrectas"
                    , Toast.LENGTH_SHORT)
                    .show()
            }
        })
    }
}
